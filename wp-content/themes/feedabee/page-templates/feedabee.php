<?php 

/* 
	Template Name: #FeedABee

*/


get_header();
?>
<div class="row">
	<h1 class="text-center title-feedabee">#FeedABee</h1>
	<div class="col-md-10  col-md-offset-1">
<!--		<p class="top-text2-feedabee">--><?php //the_field('text_top_underline'); ?><!--</p>-->
			<p class="top-text2-feedabee feedabee-textw">Explore the social impact of #FeedABee with some of the photos that have been shared on social media. 
				Join the conversation by using #FeedABee and <img src="<?php echo get_template_directory_uri(); ?>/images/bee_emoji.png" alt="bee">
			</p>
	</div>
	<div class="clearfix"></div>
	<div class="context">
		<!-- SocialBoard Start -->
		<script type="text/javascript">
			if(!window.jQuery){document.write('<script src="//www.thesocialboard.com/app/js/jquery-1.9.1.js"><\/script>');}
		</script>
		<link href="//fonts.googleapis.com/css?family=Montserrat" rel="stylesheet" type="text/css">
		<link href="//www.thesocialboard.com/app/css/board/style.css" type="text/css" rel="stylesheet">
		<link href="//www.thesocialboard.com/app/css/board/embed.css" type="text/css" rel="stylesheet">
		<script src="//bit.ly/javascript-api.js?version=latest&login=thesocialboard&apiKey=R_6366fd1a867e4735aac6ae4b04d0b33f" type="text/javascript" charset="utf-8"></script>
		<script src="//www.thesocialboard.com/app/js/board/jquery.masonry.js" type="text/javascript"></script>
		<script src="//www.thesocialboard.com/app/js/board/libs.min.js" type="text/javascript"></script>
		<script type="text/javascript">
			vkSbHandler.host_url="//www.thesocialboard.com";
			vkSbHandler.board_id=215;
			vkSbHandler.api_key="abaacd58a4f138278d5c4e125e065c13";
			vkSbHandler.page=0;vkSbHandler.external_request=true;
		</script><script src="//www.thesocialboard.com/app/js/board/init.min.js" type="text/javascript"></script>
		<script src="//www.thesocialboard.com/app/js/board/jquery.xdomain.js" type="text/javascript"></script>
		<h2 class="text-center">FIND YOUR POST BELOW</h2>
		<div class="text-center">
			<!-- <div class="vk-sb-search_button vk-sb-white my-search btn-social-search">SEARCH</div> -->
		</div>
		<div id="vk-sb-master"></div>
		<!-- SocialBoard End -->
	</div>
</div>

<script>
	$(document).ready(function(){
		$('.vk-sb-search_button').on('click', function(){
			$('#vk-sb-search-form').css('margin-top',$(window).height()/2 - 60 );
		});
	});
</script>
<?php get_footer(); ?>