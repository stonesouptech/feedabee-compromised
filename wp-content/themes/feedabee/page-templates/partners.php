<?php

/*
    Template Name: Partners

*/


get_header();

?>
    <?php while(have_posts()) : the_post(); ?>
        <div class="partners context col-sm-12 text-center">
<!--            --><?php //the_content(); ?>
            <h1>Impact</h1>
            <div class="center-container">
            <br>
                <p class="text-bold">
                    We have set out to plant forage in all 50 states by the end of 2018!
                </p>
                <p>
                    With the help of our incredible Feed a Bee partners and individuals who are passionate about pollinators, we will spread forage from coast to coast. Check in on the progress below! 
                </p>
            </div>
            <br />
            <h2 class="text-bold text-center">THE FIFTY-STATE PLANTING PROGRESS MAP</h2>
            <img class="img-responsive full-width" src="<?php echo get_template_directory_uri(); ?>/images/map-partners.png" alt="map-partners">
            <img class="img-responsive legend-img" src="<?php echo get_template_directory_uri(); ?>/images/map-legend.png" alt="legend">
            <div class="center-container">
                <h3 class="text-bold">OUR PARTNERS</h3>
            </div>
            <div class="center-container">
                <p>
                    Feed a Bee is fueled by powerful collaborations across the U.S., working to improve forage for pollinators. We have partnered with many diverse organizations across the country to plan more acres, gardens, yards, plots, and roadsides that provide bees with the food they need to thrive. View the list of Feed a Bee partners <a href="http://feedabee.stonesoupdev.com/wp-content/uploads/2016/02/FeedaBee_Partners.pdf" class="green-link text-decoration" target="_blank">here</a>.
                </p>
            </div>
            <div class="center-container">
                <span class="horz-line"></span>
            </div>
            <div class="center-container">
                <h3 class="text-bold">
                    INTERESTED IN BECOMING A FEED A BEE PARTNER? 
                </h3>
                <p>Organizations interested in planting 10 or more acres of forage should complete and return the <a href="http://feedabee.stonesoupdev.com/wp-content/uploads/2016/02/FeedaBee_OrderForm.pdf" class="green-link text-decoration" target="_blank">Feed a Bee order form</a>.</p>
                <span class="horz-line"></span>
                <h3 class="text-bold">ALREADY A FEED A BEE PARTNER?</h3>
                <p>Please use these <a target="_blank" href="http://feedabee.stonesoupdev.com/wp-content/uploads/2016/02/FeedaBee_Posters.pdf" class="green-link text-decoration">Feed a Bee posters</a> for your upcoming events.</p>
            </div>
        </div>

        <div class="clearfix"></div>
    <?php endwhile; ?>

<script>
    $(document).ready(function(){

    });
</script>

<?php get_footer(); ?>