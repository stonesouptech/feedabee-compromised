<!DOCTYPE html>
<html lang="en-US" class="no-js">
<head>
	<meta charset="UTF-8">
	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
	<meta name="description" content="Bees and pollinators need to eat! With the help of our partners and people like you, we're going to plant millions of seeds and create forage for pollinators across the country! It's easy to help plant seeds to feed a bee.">
	<meta name="keywords" content="grow,plant,free,seeds,garden,bee health,bees,pollinator,flower,wildflower">

	<link rel="profile" href="http://gmpg.org/xfn/11">
		<script>(function(html){html.className = html.className.replace(/\bno-js\b/,'js')})(document.documentElement);</script>
<title>FeedABee</title>
<link rel="alternate" type="application/rss+xml" title="FeedABee &raquo; Feed" href="http://img.feedabee.com/feed/" />
<link rel="alternate" type="application/rss+xml" title="FeedABee &raraquo; Comments Feed" href="http://img.feedabee.com/comments/feed/" />
		<script type="text/javascript">
			window._wpemojiSettings = {"baseUrl":"https:\/\/s.w.org\/images\/core\/emoji\/72x72\/","ext":".png","source":{"concatemoji":"http:\/\/img.feedabee.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.4.2"}};
			!function(a,b,c){function d(a){var c,d=b.createElement("canvas"),e=d.getContext&&d.getContext("2d"),f=String.fromCharCode;return e&&e.fillText?(e.textBaseline="top",e.font="600 32px Arial","flag"===a?(e.fillText(f(55356,56806,55356,56826),0,0),d.toDataURL().length>3e3):"diversity"===a?(e.fillText(f(55356,57221),0,0),c=e.getImageData(16,16,1,1).data.toString(),e.fillText(f(55356,57221,55356,57343),0,0),c!==e.getImageData(16,16,1,1).data.toString()):("simple"===a?e.fillText(f(55357,56835),0,0):e.fillText(f(55356,57135),0,0),0!==e.getImageData(16,16,1,1).data[0])):!1}function e(a){var c=b.createElement("script");c.src=a,c.type="text/javascript",b.getElementsByTagName("head")[0].appendChild(c)}var f,g;c.supports={simple:d("simple"),flag:d("flag"),unicode8:d("unicode8"),diversity:d("diversity")},c.DOMReady=!1,c.readyCallback=function(){c.DOMReady=!0},c.supports.simple&&c.supports.flag&&c.supports.unicode8&&c.supports.diversity||(g=function(){c.readyCallback()},b.addEventListener?(b.addEventListener("DOMContentLoaded",g,!1),a.addEventListener("load",g,!1)):(a.attachEvent("onload",g),b.attachEvent("onreadystatechange",function(){"complete"===b.readyState&&c.readyCallback()})),f=c.source||{},f.concatemoji?e(f.concatemoji):f.wpemoji&&f.twemoji&&(e(f.twemoji),e(f.wpemoji)))}(window,document,window._wpemojiSettings);
		</script>
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<link rel='stylesheet' id='juicerstyle-css'  href='//assets.juicer.io/embed.css?ver=4.4.2' type='text/css' media='all' />
<link rel='stylesheet' id='contact-form-7-css'  href='http://img.feedabee.com/wp-content/plugins/contact-form-7/includes/css/styles.css?ver=4.4' type='text/css' media='all' />
<link rel='stylesheet' id='twentysixteen-fonts-css'  href='https://fonts.googleapis.com/css?family=Merriweather%3A400%2C700%2C900%2C400italic%2C700italic%2C900italic%7CMontserrat%3A400%2C700%7CInconsolata%3A400&#038;subset=latin%2Clatin-ext' type='text/css' media='all' />
<link rel='stylesheet' id='genericons-css'  href='http://img.feedabee.com/wp-content/themes/feedabee/genericons/genericons.css?ver=3.4.1' type='text/css' media='all' />
<link rel='stylesheet' id='twentysixteen-style-css'  href='http://img.feedabee.com/wp-content/themes/feedabee/style.css?ver=4.4.2' type='text/css' media='all' />
<!--[if lt IE 10]>
<link rel='stylesheet' id='twentysixteen-ie-css'  href='http://img.feedabee.com/wp-content/themes/feedabee/css/ie.css?ver=20150930' type='text/css' media='all' />
<![endif]-->
<!--[if lt IE 9]>
<link rel='stylesheet' id='twentysixteen-ie8-css'  href='http://img.feedabee.com/wp-content/themes/feedabee/css/ie8.css?ver=20151230' type='text/css' media='all' />
<![endif]-->
<!--[if lt IE 8]>
<link rel='stylesheet' id='twentysixteen-ie7-css'  href='http://img.feedabee.com/wp-content/themes/feedabee/css/ie7.css?ver=20150930' type='text/css' media='all' />
<![endif]-->

<!-- REVOLUTION STYLE SHEETS -->
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/settings.css">
<!-- REVOLUTION LAYERS STYLES -->
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/layers.css">
<!-- REVOLUTION NAVIGATION STYLES -->
<link rel="stylesheet" type="text/css" href="<?php echo get_template_directory_uri(); ?>/css/navigation.css">

<script type='text/javascript' src='http://img.feedabee.com/wp-includes/js/jquery/jquery.js?ver=1.11.3'></script>
<script type='text/javascript' src='http://img.feedabee.com/wp-includes/js/jquery/jquery-migrate.min.js?ver=1.2.1'></script>

<!--[if lt IE 9]>
<script type='text/javascript' src='http://img.feedabee.com/wp-content/themes/feedabee/js/html5.js?ver=3.7.3'></script>
<![endif]-->
<link rel='https://api.w.org/' href='http://img.feedabee.com/wp-json/' />
<link rel="EditURI" type="application/rsd+xml" title="RSD" href="http://img.feedabee.com/xmlrpc.php?rsd" />
<link rel="wlwmanifest" type="application/wlwmanifest+xml" href="http://img.feedabee.com/wp-includes/wlwmanifest.xml" />
<meta name="generator" content="WordPress 4.4.2" />
<link rel="canonical" href="http://img.feedabee.com/" />
<link rel='shortlink' href='http://img.feedabee.com/' />
		<style type="text/css">.recentcomments a{display:inline !important;padding:0 !important;margin:0 !important;}</style>
			<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/main.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/main2.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/responsive.css">
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,800,700' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700' rel='stylesheet' type='text/css'>
	<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.ico" type="image/x-icon">
	<script src="https://code.jquery.com/jquery-2.2.0.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/main.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.js" ></script>

<meta name="twitter:card" content="player">
<meta name="twitter:url" content="http://feedabee.com">
<meta name="twitter:title" content="FeedABee">
<meta name="twitter:player" content="https://www.youtube.com/embed/7YzgYLE34TA">
<meta name="twitter:player:width" content="500">
<meta name="twitter:player:height" content="278">
<meta name="twitter:player:stream" content="https://www.youtube.com/embed/7YzgYLE34TA">
<meta name="twitter:player:stream:content_type" content="video/mp4">
<meta name="twitter:image" content="<?php echo get_template_directory_uri(); ?>/images/Twitter-Share-Card-1-Generic-560x300.jpg">

<meta property="og:url" content="http://feedabee.com">
<meta property="og:type" content="website" />
<meta property="og:title" content="FeedABee" />
<meta property="og:description" content="Watch and share the video with #FeedABee and Bayer will plant wildflowers to feed bees! With your help, we want to plant more flowers to create forage for pollinators around the U.S. Visit feedabee.com to learn more." />
<meta property="og:image" content="<?php echo get_template_directory_uri(); ?>/images/Twitter-Share-Card-1-Generic-560x300.jpg"/>
<meta property="og:type" content="video">
<meta property="og:video:url" content="https://www.youtube.com/embed/7YzgYLE34TA">
<meta property="og:video:secure_url" content="https://www.youtube.com/embed/7YzgYLE34TA">
<meta property="og:video:type" content="text/html">
<meta property="og:video:width" content="1280">
<meta property="og:video:height" content="720">
<meta property="og:video:url" content="http://www.youtube.com/v/7YzgYLE34TA?version=3&amp;autohide=1">
<meta property="og:video:secure_url" content="https://www.youtube.com/v/7YzgYLE34TA?version=3&amp;autohide=1">
<meta property="og:video:type" content="application/x-shockwave-flash">
<meta property="og:video:tag" content="FeedABee">
<meta property="fb:app_id" content="984853964977005" />




</head>

<body class="home page page-id-14 page-template page-template-page-templates page-template-home page-template-page-templateshome-php has-dashicons">
<div id="page" class="site">
	<div class="header-top">
		<div class="container">
			<div class="row">
				<div class="logo col-xs-3 col-sm-3">
					<a href="http://feedabee.com"><img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt="logo"></a>
				</div>
				<div class="col-xs-9 col-sm-9 text-right left-content-header">
					<nav class="navbar navbar-default">
						<div class="navbar-header">
					      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu-main-menu-container" aria-expanded="false">
					        <span class="sr-only">Toggle navigation</span>
					        <span class="icon-bar"></span>
					        <span class="icon-bar"></span>
					        <span class="icon-bar"></span>
					      </button>
					    </div>
					</nav>

					<div id="menu-main-menu-container" class="collapse navbar-collapse menu-container no-transition"><ul id="menu-main-menu" class="menu"><li id="menu-item-87" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-87"><a href="http://feedabee.com/">Home<li class="sep"> | </li></a></li>
					<li id="menu-item-48" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-48"><a href="<?php echo get_template_directory_uri(); ?>/feedabee/">#FeedAbee<li class="sep"> | </li></a></li>
					<li id="menu-item-47" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-47"><a href="<?php echo get_template_directory_uri(); ?>/education/">Education<li class="sep"> | </li></a></li>
					<li id="menu-item-51" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-51"><a href="<?php echo get_template_directory_uri(); ?>/impact/">Impact<li class="sep">  </li></a></li>
					</ul></div>
					<div class="bayer-logo">
						<img src="<?php echo get_template_directory_uri(); ?>/images/bayer-logo.png" alt="">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="sticky-submenu">
	   <div class="sticky-grid">
			<p>WE ARE PLANTING FORAGE FOR POLLINATORS ACROSS THE U.S.</p>
			<div class="sticky-round-border">
				 <img src="<?php echo get_template_directory_uri(); ?>/images/bee_emoji.png" alt="bee">
			</div>
			<p>YOU CAN SHARE TO HELP US PLANT MORE WILDFLOWERS</p>
	   </div>
	</div>
		<div class="container home-slider">
			<div class="heading home text-center">
				<!--Slider-->
				<div class="rev_slider_wrapper">
					<div id="rev_slider_1" class="rev_slider" style="display:none;">
						<ul>
							<li data-transition="fade" class="slide-1">
								<img src="<?php echo get_template_directory_uri(); ?>/images/feedabee_page.jpg" alt="flowers">
								<div class="tp-caption tp-resizeme largewhitebg slide-box1"

									 data-frames='[{"delay":300,"speed":1500,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.8;sY:0.8;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'
									 data-x="center"
									 data-y="center"
									 data-hoffset="0"
									 data-voffset="0"
									 data-width="['700']"
									 data-height="['auto']"

								><h3>Spread the word and we will<br /> spread wildflower seeds!</h3>
									<p>Bees and pollinators need to eat. With your help, <br /> we have set out to plant more forage for<br /> pollinators around the country.</p>
									<a href="#feedabee"><img src="<?php echo get_template_directory_uri(); ?>/images/arrow-down-slider.png" alt="arrow-down"></a>
								</div>
							</li>
							<li data-transition="fade">
								<div data-transition="fade" class="tp-caption_ slide-2"
									 data-frames='[{"delay":300,"speed":1500,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.8;sY:0.8;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'									 data-x="center"
									 data-y="center"
									 data-hoffset="0"
									 data-voffset="0"
									 data-width="['500']"
									 data-height="['auto']"
								>

									<img src="<?php echo get_template_directory_uri(); ?>/images/map.png" alt="map">
									<h2>&nbsp;We are setting out to plant <br />Feed a Bee forage in all 50<br />states by the end of 2018.</h2>
									<a href="<?php echo get_page_uri() ?>/impact">CHECK IN ON THE PROGRESS! ></a>
								</div>
							</li>
							<li data-transition="fade">
								<img src="<?php echo get_template_directory_uri(); ?>/images/new-slide-3.jpg" alt="flowers">
								<div class="tp-caption tp-resizeme slide-box3"

									 data-frames='[{"delay":500,"speed":1500,"frame":"0","from":"z:0;rX:0;rY:0;rZ:0;sX:0.8;sY:0.8;skX:0;skY:0;opacity:0;","to":"o:1;","ease":"Power4.easeOut"},{"delay":"wait","speed":300,"frame":"999","to":"auto:auto;","ease":"Power3.easeInOut"}]'									 data-x="center"
									 data-y="center"
									 data-hoffset="0"
									 data-voffset="0"
									 data-width="['500']"
									 data-height="['auto']"

								   >
								   	<img src="<?php echo get_template_directory_uri(); ?>/images/sign-3.png" alt="sign">
									<div class="popup" id="video-share">
									  <a id="twitter-video-share" target="_blank" href="https://twitter.com/intent/tweet?&text=Watch%20the%20video%20to%20help%20%23FeedABee%20%F0%9F%90%9D!%20Learn%20more%20at%20FeedABee.com&url=http://feedabee.com" class="twitter_share_button"></a>
									  <a href="javascript:void(0)" class="facebook_share_button video-facebook-share-top"></a>
									  <a id="tumblr-video-share" target="_blank" href="http://www.tumblr.com/share/video?url=https%3A%2F%2Fwww.youtube.com%2Fembed%2F7YzgYLE34TA&caption=Watch%20and%20share%20the%20video%20with%20%23FeedABee%20and%20Bayer%20will%20plant%20wildflowers%20to%20feed%20bees!%20With%20your%20help%2C%20we%20want%20to%20plant%20more%20flowers%20to%20create%20forage%20for%20pollinators%20around%20the%20U.S.%20Visit%20feedabee.com%20to%20learn%20more.&click_thru=http%3A%2F%2Fwww.feedabee.com" class="tumblr_share_button"></a>
									</div>
								</div>
							</li>

							<li data-transition="fade">
								<div class="slide-4">

									<div class="popup" id="video-share">
									  <a id="twitter-video-share" target="_blank" href="https://twitter.com/intent/tweet?&text=Watch%20the%20video%20to%20help%20%23FeedABee%20%F0%9F%90%9D!%20Learn%20more%20at%20FeedABee.com&url=http://feedabee.com" class="twitter_share_button"></a>
									  <a href="javascript:void(0)" class="facebook_share_button video-facebook-share-top"></a>
									  <a id="tumblr-video-share" target="_blank" href="http://www.tumblr.com/share/video?url=https%3A%2F%2Fwww.youtube.com%2Fembed%2F7YzgYLE34TA&caption=Watch%20and%20share%20the%20video%20with%20%23FeedABee%20and%20Bayer%20will%20plant%20wildflowers%20to%20feed%20bees!%20With%20your%20help%2C%20we%20want%20to%20plant%20more%20flowers%20to%20create%20forage%20for%20pollinators%20around%20the%20U.S.%20Visit%20feedabee.com%20to%20learn%20more.&click_thru=http%3A%2F%2Fwww.feedabee.com" class="tumblr_share_button"></a>
									</div>
									<div id="player-slider" class="player-slider"></div>
									<h2>&nbsp;Watch the video to learn about our friendly pollinators<br />and you&rsquo;ll soon be humming along with us!</h2>
								</div>
							</li>
						</ul>
					</div>
				</div>
				<!--End slider-->
<!--				<div class="underlay">-->
<!--                    <div class="post video" >-->
<!--                    	<img onclick="window.open('https://twitter.com/intent/tweet?&amp;text=%20I%20just%20helped%20%23FeedABee!%20🐝%20Watch%20the%20video%20and%20learn%20more%20at%20feedabee.com&amp;url=http://www.FeedABee.com')" src="http://img.feedabee.com/wp-content/themes/feedabee/images/end_video.PNG" class="images-video-end" alt="">-->
<!--                    	<div id="player"></div>-->
<!--                    </div>-->
					<!--<div class="share video">
						<a target="_blank" href="https://twitter.com/intent/tweet?&amp;text=%20I%20just%20helped%20%23FeedABee!%20🐝%20Watch%20the%20video%20and%20learn%20more%20at%20feedabee.com&amp;url=https://www.youtube.com/watch?v=rHKVVSSZVhU" class="share-icons twitter"></a><br>
						<a href="javascript:void(0)" class="share-icons facebook" id="facebook_video"></a><br>
						<a target="_blank" class="share-icons tumblr" href="http://www.tumblr.com/share/video?embed=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DrHKVVSSZVhU&amp;caption=Learn+how+you+can+help+%23FeedABee+at+www.FeedaBee.com+🐝&amp;click_thru=http%3A%2F%2Fwww.feedabee.com"></a><br>
					</div> -->
<!--				</div>-->
			</div>
		</div>
	</div>
<!--	<div class="icons">-->
<!--		<div class="icon beehalf" onclick="window.open('https://twitter.com/intent/tweet?&amp;text=I+just+helped+%23FeedABee%21+%F0%9F%90%9D+Learn+how+you+can+too+at+feedabee.com&amp;url=http://goo.gl/jYvevu')">-->
<!--			TWEET A BEE <br>FEED A BEE-->
<!--		</div>-->
<!--	</div>-->
	<div class="sticky-submenu sticky-responsive">
		<div class="sticky-grid-resp">
			<p>WE ARE PLANTING<br /> FORAGE FOR<br /> POLLINATORS<br /> ACROSS THE U.S.</p>
			<div class="sticky-round-border">
				<img src="<?php echo get_template_directory_uri(); ?>/images/bee_emoji.png" alt="bee">
			</div>
			<p>YOU CAN SHARE TO<br /> HELP US PLANT<br /> MORE<br /> WILDFLOWERS</p>
		</div>
	</div>
	<div class="section feedabee" id="feedabee">
		<div class="inner text-center home-content">
			<h2>Every time  <span>#FeedABee</span> <img src="<?php echo get_template_directory_uri(); ?>/images/bee_emoji.png" alt="bee"> is shared,
				we&rsquo;ll plant a wildflower!
			</h2>
			<p>Feed a bee by sharing one of the items below. Every time you WATCH and SHARE, we will plant more forage for pollinators.</p>
		</div>
		<div class="home-tabs-section">
			<div class="bee-home-tab pull-left">
				<a href="<?php echo get_home_url(); ?>/impact" style="color: #333;">
					<img src="<?php echo get_template_directory_uri(); ?>/images/map.png" alt="map">
					<p>WE ARE SETTING OUT TO PLANT FEED A BEE FORAGE IN ALL 50 STATES BT THE END OF 2018.
						CHECK IN ON THE PROGRESS >
					</p>
				</a>
			</div>
			<div class="bee-home-tab billion-trigger">
				<img src="<?php echo get_template_directory_uri(); ?>/images/2billion-flowers.png" alt="flowers">
				<p>THANKS TO YOUR SUPPORT, WE HAVE PLANTED OVER TWO BILLION WILDFLOWERS IN THE LAST TWO YEARS.
					SHARE THE BUZZ!
				</p>
			</div>
			<div class="bee-home-tab pull-right tab-player-container">
				<img class="tab-player" src="<?php echo get_template_directory_uri(); ?>/images/video-player.png" alt="player">
				<p>WATCH THE VIDEO TO LEARN, ABOUT OUR FRIENDLY POLLINATORS AND YOU'LL
					SOON BE HUMMING ALONG WITH US!
				</p>
			</div>
			<!--Tablet Responsive only-->
			<div class="bee-home-tab-tablet white-background pull-left">
				<img src="<?php echo get_template_directory_uri(); ?>/images/map.png" alt="map">
			</div>
			<div class="bee-home-tab-tablet pull-right">
				<a href="<?php echo get_home_url(); ?>/impact" style="color: #333;">
					<p>WE ARE SETTING OUT TO PLANT FEED A BEE FORAGE IN ALL 50 STATES BT THE END OF 2018.
						CHECK IN ON THE PROGRESS >
					</p>
				</a>
			</div>
			<div class="bee-home-tab-tablet pull-left billion-trigger">
				<p>THANKS TO YOUR SUPPORT, WE HAVE PLANTED OVER TWO BILLION WILDFLOWERS IN THELAST TWO YEARS.
					SHARE THE BUZZ!
				</p>
			</div>
			<div class="bee-home-tab-tablet white-background pull-right">
				<img src="<?php echo get_template_directory_uri(); ?>/images/2billion-flowers.png" alt="flowers">
			</div>
			<div class="bee-home-tab-tablet white-background pull-left tab-player-container">
				<img class="tab-player tablet-t-player" src="<?php echo get_template_directory_uri(); ?>/images/video-player.png" alt="player">
			</div>
			<div class="bee-home-tab-tablet pull-right">
				<p>WATCH THE VIDEO TO LEARN, ABOUT OUR FRIENDLY POLLINATORS AND YOU'LL
					SOON BE HUMMING ALONG WITH US!
				</p>
			</div>
			<!--End tablet responsive only-->
		</div>
	<div class="clearfix"></div>
	<div class="container">
		<div class="footer-share">
			<div class="footer-top-el">
				<h3>#FEEDABEE</h3>
				<img class="bee-footer-icon resp-bee-icon" src="<?php echo get_template_directory_uri(); ?>/images/bw-bee.png" alt="bee">
			</div>
			<ul>
				<li><img class="bee-footer-icon" src="<?php echo get_template_directory_uri(); ?>/images/bw-bee.png" alt="bee"></li>
				<li><a class="twitter-footer-home" target="_blank" href="https://twitter.com/intent/tweet?&text=Spread%20the%20word%20%26%20retweet%20to%20%23FeedABee%20%F0%9F%90%9D&url=https://goo.gl/8vL4N9"><img class="hvr-grow" src="<?php echo get_template_directory_uri(); ?>/images/twitter-black.png" alt="twitter"></a></li>
				<li><a href="javascript:void(0)" class="facebook-footer-home"><img class="hvr-grow" src="<?php echo get_template_directory_uri(); ?>/images/facebook-black.png" alt="facebook"></a></li>
				<li><a class="tumblr-footer-home" target="_blank" href="http://www.tumblr.com/share/photo?source=http%3A%2F%2Ffeedabee.stonesoupdev.com%2Fwp-content%2Fthemes%2Ffeedabee%2Fimages%2FFacebook-Share-Card-1-Generic-240x240.jpg&caption=Every%20time%20%23FeedABee%20%F0%9F%90%9D%20is%20shared%2C%20we%E2%80%99ll%20plant%20a%20wildflower.%20With%20your%20help%2C%20we%20want%20to%20plant%20more%20flowers%20to%20create%20forage%20for%20pollinators%20around%20the%20U.S.&click_thru=http%3A%2F%2Fwww.feedabee.com"><img class="hvr-grow" src="<?php echo get_template_directory_uri(); ?>/images/tumblr-black.png" alt="tumblr"></a></li>

			</ul>
		</div>
		<div class="footer text-center">
			<p style="font-size:14px;">Two billion wildflowers seeds have been distributed in the U.S. since 2015.</p>
		</div>
		<div class="footer text-center">
			Bayer CropScience LP, 2 T.W. Alexander Drive, Research Triangle Park, N.C. 27709. Always read and follow label instructions. Bayer and the Bayer Cross are registered trademarks of Bayer. For product information, call toll-free 1-866-99-BAYER (1-866-992-2937) or visit our website at <a href="http://www.BayerCropScience.us" target="_blank">www.BayerCropScience.us</a>
		</div>
	</div>
</div> <!-- /div#page -->
<div class="overlay"></div>
<div class="popup commit-popup">
	<div class="popup-header text-right"><span class="glyphicon glyphicon-remove close-popup"></span></div>
	<div class="popup-body text-center">
	<img src="http://img.feedabee.com/wp-content/themes/feedabee/images/new_pop.jpg" alt="">
		<div class="share">
			<div class="share-text">Share the Buzz</div>
			<hr class="sep">
			<a target="_blank" class="share-icons twitter" href="https://twitter.com/intent/tweet?&amp;text=%20I%20just%20helped%20%23FeedABee%20%26%20you%20can%20too!%20Visit%20www.FeedABee.com%20and%20watch%20the%20video%20to%20learn%20more+🐝&url=http://goo.gl/7mK4Jd" target="_blank"></a>
			<a href="javascript:void(0)" class="share-icons facebook" id="facebook_share_buzz"></a>
			<a target="_blank" class="share-icons tumblr" href="http://www.tumblr.com/share/photo?source=http%3A%2F%2Fimg.feedabee.com%2Fwp-content%2Fthemes%2Ffeedabee%2Fimages%2FFacebook-Share-Card-1-Generic-240x240.jpg&caption=I+just+helped+%23FeedABee+%26+you+can+too%21+Visit+www.FeedABee.com+and+watch+the+video+to+learn+how+you+can+support+our+pollinators+🐝&click_thru=http%3A%2F%2Fwww.feedabee.com"></a>
		</div>
	</div>
	<div class="popup-footer"></div>
</div>
<div class="popup beehalf-popup">
	<div class="popup-header text-right">
		<span class="glyphicon glyphicon-remove close-popup"></span>
		<h3 class="text-center">Want to participate but don't have a green thumb?</h3>
	</div>
	<div class="popup-body text-center">
		Let us plant seeds on your 'bee-half'. Provide your name and email address, and your seed packet will help us plant more flowers for bees. And don't forget to check back in to watch our progress grow!
		<!--
		<input type="text" name="name" placeholder="NAME" class="text-center">
		<input type="text" name="email" placeholder="EMAIL" class="text-center">
		<button type="submit" class="button-default">Submit</button>
		<div class="updates"><input type="checkbox" name="newsletter" id="newsletter" checked><label for="newsletter"><div class="checkbox"><span class="glyphicon glyphicon-remove"></span></div></label> I would like to receive bee updates from Bayer</div> -->
		<div role="form" class="wpcf7" id="wpcf7-f153-o1" lang="en-US" dir="ltr">
<div class="screen-reader-response"></div>
<form action="/#wpcf7-f153-o1" method="post" class="wpcf7-form" novalidate="novalidate">
<div style="display: none;">
<input type="hidden" name="_wpcf7" value="153" />
<input type="hidden" name="_wpcf7_version" value="4.4" />
<input type="hidden" name="_wpcf7_locale" value="en_US" />
<input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f153-o1" />
<input type="hidden" name="_wpnonce" value="41e2cfddce" />
</div>
<p><span class="wpcf7-form-control-wrap name"><input type="text" name="name" value="" size="40" class="wpcf7-form-control wpcf7-text text-center" aria-invalid="false" placeholder="NAME" /></span><br />
<span class="wpcf7-form-control-wrap email"><input type="text" name="email" value="" size="40" class="wpcf7-form-control wpcf7-text text-center" aria-invalid="false" placeholder="EMAIL" /></span><br />
<span class="wpcf7-form-control-wrap address"><input type="text" name="address" value="" size="40" class="wpcf7-form-control wpcf7-text text-center" aria-invalid="false" placeholder="STREET ADDRESS" /></span><br />
<span class="wpcf7-form-control-wrap city"><input type="text" name="city" value="" size="40" class="wpcf7-form-control wpcf7-text text-center" aria-invalid="false" placeholder="CITY" /></span><span class="wpcf7-form-control-wrap state"><input type="text" name="state" value="" size="40" class="wpcf7-form-control wpcf7-text text-center" aria-invalid="false" placeholder="STATE" /></span><span class="wpcf7-form-control-wrap zip"><input type="text" name="zip" value="" size="40" class="wpcf7-form-control wpcf7-text text-center" aria-invalid="false" placeholder="ZIP" /></span><button type="submit" class="button-default">Submit</button>
<div class="updates"><input type="checkbox" name="address" id="address" checked><label for="address">
<div class="checkbox"><span class="glyphicon glyphicon-remove"></span></div>
<p></label> I would like to receive bee updates from Bayer</div>
<div class="wpcf7-response-output wpcf7-display-none"></div></form></div>
	</div>
</div>
<div class="popup freeseeds-popup" style="height: 530px;">
	<div class="popup-header text-right">
		<span class="glyphicon glyphicon-remove close-popup"></span>
	<h3 class="text-center">Get your free packet of seeds to plant!</h3>
	</div>
	<iframe src="http://img.feedabee.com/popup-iframe" style="width:100%;overflow:hidden;height: 440px;border:none"></iframe>
</div>

<div class="popup commit-grow-popup" style="height: 380px;" id="us">
	<div class="popup-header text-right">
		<span class="glyphicon glyphicon-remove close-popup"></span>
	<h3 class="text-center">Commit to growing a plant</h3>
	</div>
	<iframe src="http://img.feedabee.com/popup-iframe-2" style="width:100%;overflow:hidden;height: 310px;border:none"></iframe>
</div>

<div class="popup commit-grow-popup" style="" id="not-us">
	<div class="popup-header text-right">
		<span class="glyphicon glyphicon-remove close-popup"></span>
	<h3 class="text-center">Commit to growing a plant</h3>
	</div>
	<div class="popup-body text-center" style="background-color: #1accca;font-family: 'PT Sans Caption', sans-serif;padding: 20px; color: #fff;">
		Thank you for joining Feed a Bee and supporting pollinator health in your own neighborhood.  Don’t forget to check back in for planting tips. Please share photos of progress of your seeds using #FeedABee.
	</div>
</div>

<div class="popup not-us" style="">
	<div class="popup-header text-right">
		<span class="glyphicon glyphicon-remove close-popup"></span>
	</div>
	<div class="popup-body text-center">
		<p>Sorry, seeds are only for order in the U.S. But please support Feed a Bee by tweeting #FeedABee!</p>
		<button class="btn-default">CLOSE</button>
	</div>
</div>

<div class="popup thankyou-popup">
	<div class="popup-header text-right"><span class="glyphicon glyphicon-remove close-popup"></span></div>
	<div class="popup-body text-center">
		<div class="share">
			<h3 class="text-center">Thank you for <br> helping #FEEDABEE!</h3>
			<p>With your help, we are increasing forage for pollinators. Share the buzz with your friends and lets keep planting!</p>
			<div class="share-text">Share the Buzz</div>
			<hr class="sep">
			<a target="_blank" href="https://twitter.com/intent/tweet?&amp;text=%20I%20just%20helped%20%23FeedABee%20%26%20you%20can%20too!%20Visit%20www.FeedABee.com%20and%20watch%20the%20video%20to%20learn%20more+🐝&url=http://goo.gl/K1ooAv" class="share-icons twitter"></a>
			<a href="javascript:void(0)" class="share-icons facebook" id="facebook_thank"></a>
			<a class="share-icons tumblr" target="_blank" href="http://www.tumblr.com/share/photo?source=http%3A%2F%2Fimg.feedabee.com/%2Fcards%2Fpopup2.jpg&caption=I+just+helped+%23FeedABee+%26+you+can+too%21+Visit+www.FeedABee.com+and+watch+the+video+to+learn+how+you+can+support+our+pollinators+🐝&click_thru=http%3A%2F%2Fwww.feedabee.com"></a>
		</div>
	</div>
	<div class="popup-footer"></div>
</div>


<div class="youtube-share modal_popout">
	<div class="pull-left">
		<img src="<?php echo get_template_directory_uri()?>/images/logo-white.png" alt="bayer">
		<h2>Feed a Bee</h2>
	</div>
	<div class="clearfix"></div>
	<span></span>
	<div class="right-share">
		<h1>Share the Buzz!</h1>
		<p>Share the video with<br />#FeedABee <img class="s-bee-img" src="<?php echo get_template_directory_uri()?>/images/bee_emoji.png" alt="ee">
			and we<br /> will plant a wildflower<br />on your bee-half!
		</p>
		<ul>
			<li><a href="javascript:void(0)"><img class="hvr-grow" src="<?php echo get_template_directory_uri(); ?>/images/twitter-black.png" alt="twitter"></a></li>
			<li><a href="javascript:void(0)"><img class="hvr-grow" src="<?php echo get_template_directory_uri(); ?>/images/facebook-black.png" alt="facebook"></a></li>
			<li><a href="javascript:void(0)"><img class="hvr-grow" src="<?php echo get_template_directory_uri(); ?>/images/tumblr-black.png" alt="tumblr"></a></li>
		</ul>
	</div>
</div>

<!-- sharing / thanks / popups -->

<div class="popup" id="popup-thanks-video">
  <a href="javascript:void(0)" class="close_top"></a>
  <a href="javascript:void(0)" class="close_bottom"></a>
</div>

<div class="popup" id="popup-thanks-generic">
  <a href="javascript:void(0)" class="close_top"></a>
  <a href="javascript:void(0)" class="close_bottom"></a>
</div>

<div class="popup" id="popup-share">
  <a href="javascript:void(0)" class="close_top"></a>
  <a target="_blank" href="https://twitter.com/intent/tweet?&text=Through%20%23FeedABee%20%F0%9F%90%9D%202%20billion%20wildflowers%20were%20planted%20to%20support%20pollinators%21&url=https://goo.gl/92gnhx" class="billion-twitter twitter_share_button"></a>
  <a href="javascript:void(0)" class="billion-facebook facebook_share_button"></a>
  <a target="_blank" href="http://www.tumblr.com/share/photo?source=http%3A%2F%2Ffeedabee.stonesoupdev.com%2Fwp-content%2Fthemes%2Ffeedabee%2Fimages%2FFacebook-Share-Card-2-2-BILLION-FLOWERS-240x240.jpg&caption=In%202016%2C%20%23FeedABee%20%F0%9F%90%9D%20planted%20over%202%20billion%20wildflowers%20that%20support%20bees%20and%20other%20pollinators.%20Celebrate%20with%20us%20by%20learning%20how%20we%E2%80%99re%20planning%20to%20spread%20more%20forage%20in%202017%21&click_thru=http%3A%2F%2Ffeedabee.stonesoupdev.com" class="tumblr_share_button billion-tumblr"></a>
</div>


<!-- sharing / thanks / popups -->

<!--Slider-->
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/jquery.themepunch.revolution.min.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/revolution.extension.actions.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/revolution.extension.layeranimation.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/revolution.extension.video.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/revolution.extension.navigation.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/revolution.extension.slideanims.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/revolution.extension.migration.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/revolution.extension.kenburn.js"></script>
<script type="text/javascript" src="<?php echo get_template_directory_uri(); ?>/js/revolution.extension.parallax.js"></script>
<script>
  // 2. This code loads the IFrame Player API code asynchronously.
  var tag = document.createElement('script');

  tag.src = "https://www.youtube.com/iframe_api";
  var firstScriptTag = document.getElementsByTagName('script')[0];
  firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

  // 3. This function creates an <iframe> (and YouTube player)
  //    after the API code downloads.
  var player;
  var playerSlider;
  var playerTablet;
  var showVideoShare = 1;

	/*sharing / thanks / popups*/

	function show_popup(popup) {
		if (popup) {
			popup.fadeIn();
		}
	}

	function hide_popup(popup) {
		if (popup) {
			popup.fadeOut();
		}
	}

	$(document).on("click",".popup",function() {
		hide_popup($(this));
	});

	$(document).on("click",".billion-trigger",function() {
		show_popup($('#popup-share'));
	});

	//show_popup($('#popup-thanks-video'));
	//show_popup($('#popup-thanks-generic'));

	/*sharing / thanks / popups*/


  function onYouTubeIframeAPIReady() {
	  playerSlider = new YT.Player('player-slider', {
		  height: '380',
		  width: '670',
		  videoId: '7YzgYLE34TA',
		  playerVars: {
			  'autoplay': 0,
			  'rel': 0,
			  'showinfo': 0,
		  },
		  events: {
			  'onStateChange': onPlayerStateChange
		  }
	  });
  }


 /* function onPlayerStateChange(event) {
    if (event.data == YT.PlayerState.ENDED || event.data == YT.PlayerState.PAUSED ) {

		if (showVideoShare) show_popup($('#video-share'));
    } else {
    	showVideoShare = 1;
		$('#rev_slider_1').revpause();
		hide_popup($('#video-share'));
	}
  }*/


    var done = false;
    var pauseFlag = true;
    
    function onPlayerStateChange(event) {
    if (event.data == YT.PlayerState.ENDED ) {

	    if (showVideoShare) show_popup($('#video-share'));

	    ga('send', {
		  hitType: 'event',
		  eventCategory: 'Video',
		  eventAction: 'stops',
		  eventLabel: 'FAB Video'
		});

	    duration = event.target.getCurrentTime();

		ga('send', {
		  hitType: 'event',
		  eventCategory: 'Video',
		  eventAction: 'views',
		  eventLabel: 'FAB Video',
		  'eventValue': parseInt(duration),
		});

    } else if (event.data == YT.PlayerState.PAUSED && pauseFlag) {

	    if (showVideoShare) show_popup($('#video-share'));

        ga('send', {
		  hitType: 'event',
		  eventCategory: 'Video',
		  eventAction: 'stops',
		  eventLabel: 'FAB Video'
		});
		pauseFlag = false;

		duration = event.target.getCurrentTime();

		ga('send', {
		  hitType: 'event',
		  eventCategory: 'Video',
		  eventAction: 'views',
		  eventLabel: 'FAB Video',
		  'eventValue': parseInt(duration),
		});

    } else if( event.data = YT.PlayerState.PLAYING ){

    	showVideoShare = 1;
		$('#rev_slider_1').revpause();
		hide_popup($('#video-share'));

		ga('send', {
		  hitType: 'event',
		  eventCategory: 'Video',
		  eventAction: 'plays',
		  eventLabel: 'FAB Video'
		});
		pauseFlag = true;
	}
  }

</script>
<script>

var isUS = 0;

$(document).ready(function(){
	var eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
	var eventer = window[eventMethod];
	var messageEvent = eventMethod == "attachEvent" ? "onmessage" : "message";

	// Listen to message from child window
	eventer(messageEvent,function(e) {
	    var key = e.message ? "message" : "data";
	    var data = e[key];
	   if(data == 'closePopUp'){
	    	$('.freeseeds-popup .close-popup').click();
	    }
	    //run function//
	},false);
});



if(location.hostname == 'feedabee.com'){
  window.fbAsyncInit = function() {
     FB.init({
	      appId      : '1148056401919842',
	      xfbml      : true,
	      version    : 'v2.5'
	    });

  };
}else{
	window.fbAsyncInit = function() {
	   FB.init({
	      appId      : '769761559841387',
	      xfbml      : true,
	      version    : 'v2.5'
	    });
	};
}

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>
<script type="text/javascript">
  $(document).ready(function(){

  	 $.ajax({
    	url: 'http://ip-api.com/json',
    	method: 'GET',
    	success: function( data ){
    		if( data.countryCode == 'US' ){
    			isUS = 1;
    		}else{
    			isUS = 0;
    		}
    	},
    	error: function(data){
    		isUS = 1;
    	}
    });



  	 $('.billion-twitter').click(function(e){
  	  	 show_popup($('#popup-thanks-generic'));
	       ga('send', {
			  hitType: 'event',
			  eventCategory: 'Shares',
			  eventAction: 'shareTwitter',
			  eventLabel: 'share 2 Billion Twitter Home'
			});
  	 });
  	 $('.billion-tumblr').click(function(e){
  	  	 show_popup($('#popup-thanks-generic'));
	       ga('send', {
			  hitType: 'event',
			  eventCategory: 'Shares',
			  eventAction: 'shareTumblr',
			  eventLabel: 'share 2 Billion Tumblr Home'
			});
  	 });


  	 $('.twitter-footer-home').click(function(e){
  	  	 show_popup($('#popup-thanks-generic'));
	       ga('send', {
			  hitType: 'event',
			  eventCategory: 'Shares',
			  eventAction: 'shareTwitter',
			  eventLabel: 'share Footer Twitter Home'
			});
  	 });
  	 $('.tumblr-footer-home').click(function(e){
  	  	 show_popup($('#popup-thanks-generic'));
  	  	 ga('send', {
			  hitType: 'event',
			  eventCategory: 'Shares',
			  eventAction: 'shareTumblr',
			  eventLabel: 'share Footer Tumblr Home'
			});
  	 });


  	 $('#twitter-video-share, #tumblr-video-share').click(function(e){
  	  	 show_popup($('#popup-thanks-video'));

  	  	 ga('send', {
		  hitType: 'event',
		  eventCategory: 'Video',
		  eventAction: 'shares',
		  eventLabel: 'FAB Video'
		});
  	 });


     $('.video-facebook-share-top').click(function(e){
      e.preventDefault();
      FB.ui(
        {
          method: 'feed',
          name: 'FeedABee',
          link: 'https://youtu.be/7YzgYLE34TA',
          picture: '<?php echo get_template_directory_uri(); ?>/images/video_card.jpg',
          caption: 'FeedABee.com',
          description: 'Watch and share the video with #FeedABee and Bayer will plant wildflowers to feed bees! With your help, we want to plant more flowers to create forage for pollinators around the U.S. Visit feedabee.com to learn more.',
          message: 'Watch the video to help #FeedABee 🐝! Learn more at FeedABee.com'
        }
      );
      show_popup($('#popup-thanks-video'));
      ga('send', {
		  hitType: 'event',
		  eventCategory: 'Video',
		  eventAction: 'shares',
		  eventLabel: 'FAB Video'
		});
    });

     $('.facebook-footer-home').click(function(e){
      e.preventDefault();
      FB.ui(
        {
          method: 'feed',
          name: 'FeedABee',
          link: 'http://feedabee.com',
          picture: '<?php echo get_template_directory_uri(); ?>/images/Facebook-Share-Card-1-Generic-240x240.jpg',
          caption: 'FeedABee.com',
          description: 'Every time #FeedABee is shared, we’ll plant a wildflower. With your help, we want to plant more flowers to create forage for pollinators around the U.S. ',
          message: 'FeedABee'
        }
      );
      show_popup($('#popup-thanks-generic'));
      ga('send', {
		  hitType: 'event',
		  eventCategory: 'Shares',
		  eventAction: 'shareFacebook',
		  eventLabel: 'share Footer Facebook Home'
		});
    });

    $('.billion-facebook').click(function(e){
      e.preventDefault();
      FB.ui(
        {
          method: 'feed',
          name: 'FeedABee Plants 2 Billion',
          link: 'http://feedabee.com',
          picture: '<?php echo get_template_directory_uri(); ?>/images/Facebook-Share-Card-2-2-BILLION-FLOWERS-240x240.jpg',
          caption: 'FeedABee.com',
          description: 'In 2016, #FeedABee planted over 2 billion wildflowers that support bees and other pollinators. Celebrate with us by learning how we’re planning to spread more forage in 2017!',
          message: '#FeedABee 🐝'
        }
      );
      show_popup($('#popup-thanks-generic'));
       ga('send', {
		  hitType: 'event',
		  eventCategory: 'Shares',
		  eventAction: 'shareFacebook',
		  eventLabel: 'share 2 Billion Facebook Home'
		});
    });
/*
     $('#facebook_share_buzz').click(function(e){
      e.preventDefault();
      FB.ui(
        {
          method: 'feed',
          name: 'HELP INCREASE THE NUMBER OF BEES BY PLANTING ONE OF THESE POLINATOR FRIENDLY FLOWERS',
          link: 'http://feedabee.com',
          picture: 'http://img.feedabee.com/wp-content/themes/feedabee/images/FAB-list-of-seeds-social-share-from-pop-up.jpg',
          caption: 'FeedABee',
          description: 'I just helped #FeedABee & you can too! Visit www.FeedABee.com and watch the video to learn how you can support our pollinators🐝',
          message: ''
        }
      );
    });

    $('#facebook_video').click(function(e){
      e.preventDefault();
      FB.ui(
        {
          method: 'feed',
          name: 'I just helped #FeedABee! 🐝 Watch the video and learn more at feedabee.com',
          link: 'https://www.youtube.com/watch?v=rHKVVSSZVhU',
          picture: 'http://img.feedabee.com/wp-content/themes/feedabee/images/playbutton.jpg',
          caption: 'FeedABee',
          description: 'I just helped #FeedABee & you can too! Visit www.FeedABee.com and watch the video to learn how you can support our pollinators🐝',
          message: ''
        }
      );
    });

    $('#facebook_thank').click(function(e){
      e.preventDefault();
      FB.ui(
        {
          method: 'feed',
          name: 'THANK YOU FOR HELPING #FEEDABEE!',
          link: 'http://feedabee.com',
          picture: 'http://img.feedabee.com/wp-content/themes/feedabee/images/FAB-thank-you-social-share-from-pop-up.jpg',
          caption: 'FeedABee',
          description: 'I just helped #FeedABee & you can too! Visit www.FeedABee.com and watch the video to learn how you can support our pollinators🐝',
          message: ''
        }
      );
    });*/

  });
</script>
<script>
/*modified*/

	$('.icon.commit').on('click', function(){
		$('.overlay').fadeIn(300);
		$('.overlay').removeClass("freeseeds-overlay");
		$('.overlay').removeClass("thanks-overlay");
		$('.overlay').removeClass('list-overlay');
		$('.overlay').removeClass("not-us-overlay");
		$('.overlay').addClass("commit-overlay");
		if( isUS ){
			$('.popup.commit-grow-popup#us').fadeIn(300);
		}else{
			$('.popup.commit-grow-popup#not-us').fadeIn(300);
		}
	});

	$('.commit-grow-popup .close-popup').on('click', function(){
		if( $(".overlay").hasClass("commit-overlay") ){
			$('.commit-grow-popup').fadeOut(300);
			$('.commit-popup').fadeIn(300);
			$('.overlay').removeClass("freeseeds-overlay");
			$('.overlay').removeClass("commit-overlay");
			$('.overlay').removeClass('thanks-overlay');
			$('.overlay').removeClass("not-us-overlay");
			$('.overlay').addClass('list-overlay');
		}
	});

	$('body').on('click', '.commit-overlay' , function(){
		if( $(".overlay").hasClass("commit-overlay") ){
			$('.commit-grow-popup').fadeOut(300);
			$('.commit-popup').fadeIn(300);
			$('.overlay').removeClass("freeseeds-overlay");
			$('.overlay').removeClass("commit-overlay");
			$('.overlay').removeClass('thanks-overlay');
			$('.overlay').removeClass("not-us-overlay");
			$('.overlay').addClass('list-overlay');
		}
	});

	$('.commit-popup .close-popup').on('click', function(){
		if( $(".overlay").hasClass("list-overlay") ){
			$('.commit-popup').fadeOut(300);
			$('.list-overlay').fadeOut(300);
			$('.overlay').removeClass("freeseeds-overlay");
			$('.overlay').removeClass("commit-overlay");
			$('.overlay').removeClass('thanks-overlay');
			$('.overlay').removeClass('list-overlay');
			$('.overlay').removeClass("not-us-overlay");
		}
	});

	$('body').on('click', '.list-overlay' , function(){
		if( $(".overlay").hasClass("list-overlay") ){
			$('.commit-popup').fadeOut(300);
			$('.list-overlay').fadeOut(300);
			$('.overlay').removeClass("freeseeds-overlay");
			$('.overlay').removeClass("commit-overlay");
			$('.overlay').removeClass('thanks-overlay');
			$('.overlay').removeClass('list-overlay');
			$('.overlay').removeClass("not-us-overlay");
		}
	});

	$('.icon.freeseeds').on('click', function(){
		if( isUS ){
			$('.overlay').fadeIn(300);
			$('.overlay').removeClass("commit-overlay");
			$('.overlay').removeClass('freeseeds-overlay');
			$('.overlay').removeClass('list-overlay');
			$('.overlay').removeClass("not-us-overlay");
			$('.overlay').addClass("thanks-overlay");
			$('.thankyou-popup').fadeIn(300);
		}else{
			$('.overlay').fadeIn(300);
			$('.overlay').removeClass("commit-overlay");
			$('.overlay').removeClass('not-us-overlay');
			$('.overlay').removeClass('list-overlay');
			$('.overlay').removeClass("freeseeds-overlay");
			$('.overlay').addClass("thanks-overlay");
			$('.thankyou-popup').fadeIn(300);
		}
	});

	$('.freeseeds-popup .close-popup').on('click', function(){
		if( $(".overlay").hasClass("freeseeds-overlay") ){
			$('.freeseeds-popup').fadeOut(300);
			$('.thankyou-popup').fadeIn(300);
			$('.overlay').removeClass("freeseeds-overlay");
			$('.overlay').removeClass("commit-overlay");
			$('.overlay').removeClass('list-overlay');
			$('.overlay').addClass('thanks-overlay');
			$('.overlay').removeClass("not-us-overlay");
		}
	});


	$('body').on('click', '.freeseeds-overlay' , function(){
		if( $(".overlay").hasClass("freeseeds-overlay") ){
			$('.freeseeds-popup').fadeOut(300);
			$('.thankyou-popup').fadeIn(300);
			$('.overlay').removeClass("freeseeds-overlay");
			$('.overlay').removeClass("commit-overlay");
			$('.overlay').removeClass('list-overlay');
			$('.overlay').addClass('thanks-overlay');
			$('.overlay').removeClass("not-us-overlay");
		}
	});

	$('.thankyou-popup .close-popup').on('click', function(){
		if( $(".overlay").hasClass("thanks-overlay") ){
			$('.thankyou-popup').fadeOut(300);
			$('.thanks-overlay').fadeOut(300);
			$('.overlay').removeClass("freeseeds-overlay");
			$('.overlay').removeClass("commit-overlay");
			$('.overlay').removeClass('thanks-overlay');
			$('.overlay').removeClass('list-overlay');
			$('.overlay').removeClass("not-us-overlay");
		}
	});

	$('body').on('click', '.thanks-overlay' , function(){
		if( $(".overlay").hasClass("thanks-overlay") ){
			$('.thankyou-popup').fadeOut(300);
			$('.thanks-overlay').fadeOut(300);
			$('.overlay').removeClass("freeseeds-overlay");
			$('.overlay').removeClass("commit-overlay");
			$('.overlay').removeClass('thanks-overlay');
			$('.overlay').removeClass('list-overlay');
			$('.overlay').removeClass("not-us-overlay");
		}
	});

	$('.not-us .close-popup, .not-us button').on('click', function(){
		if( $(".overlay").hasClass("not-us-overlay") ){
			$('.not-us').fadeOut(300);
			$('.not-us-overlay').fadeOut(300);
			$('.overlay').removeClass("freeseeds-overlay");
			$('.overlay').removeClass("commit-overlay");
			$('.overlay').removeClass('thanks-overlay');
			$('.overlay').removeClass('list-overlay');
			$('.overlay').removeClass("not-us-overlay");
		}
	});

	$('body').on('click', '.not-us-overlay' , function(){
		if( $(".overlay").hasClass("not-us-overlay") ){
			$('.not-us').fadeOut(300);
			$('.not-us-overlay').fadeOut(300);
			$('.overlay').removeClass("freeseeds-overlay");
			$('.overlay').removeClass("commit-overlay");
			$('.overlay').removeClass('thanks-overlay');
			$('.overlay').removeClass('list-overlay');
			$('.overlay').removeClass("not-us-overlay");
		}
	});

	var slider = $('#rev_slider_1');

	slider.on('hover click', function(){
		$(this).revpause();
	});

	$('.tab-player-container').on('click', function() {
		slider.revshowslide(4);
		$("html, body").animate({ scrollTop: 0 }, "slow");
		playerSlider.playVideo();
	});

	$(document).ready(function() {
		slider.show().revolution({
			sliderType:"standard",
			sliderLayout:"auto",
			gridheight: 600,
			minHeight: 450,
			delay:8000,
			responsiveLevels: [1240, 1024, 768, 414, 375, 320],
			navigation: {
				keyboardNavigation: "on",
				keyboard_direction: "horizontal",
				mouseScrollNavigation: "off",
				mouseScrollReverse: "default",
				onHoverStop: "off",
				touch: {
					touchenabled: "on",
					swipe_threshold: 75,
					swipe_min_touches: 1,
					swipe_direction: "horizontal",
					drag_block_vertical: true
				},
				arrows: {
					style:"hesperiden",
					enable:true,
					hide_onmobile:false,
					hide_under:0,
					hide_onleave:true,
					hide_delay:300,
					hide_delay_mobile:300,
					tmp:'<div class="tp-title-wrap">  	<div class="tp-arr-imgholder"></div> </div>',
					left: {
						container: 'slider',
						h_align:"left",
						v_align:"center",
						h_offset:30,
						v_offset:0
					},
					right: {
						container: 'slider',
						h_align:"right",
						v_align:"center",
						h_offset:30,
						v_offset:0
					}
				},
				bullets: {
					enable:true,
					hide_onmobile:false,
					hide_under:600,
					style:"hesperiden",
					hide_onleave:true,
					hide_delay:300,
					hide_delay_mobile:300,
					direction:"horizontal",
					h_align:"center",
					v_align:"bottom",
					h_offset:0,
					v_offset:30,
					space:5,
					tmp:'<span class="tp-bullet-img-wrap"></span>'
				}
			},

		});
	});
	$(document).on("click",".tp-arr-imgholder, .tp-bullet",function() {
		setTimeout('slider.revpause()',1000);
		if (slider.revcurrentslide() == 4){
			showVideoShare = 0;
			playerSlider.pauseVideo();
			hide_popup($('#vide-share'));
		}
	});

	$(document).on("tap",".rev_slider_wrapper",function(){
		setTimeout('slider.revpause()',1000);
	});

	var size = [window.width,window.height];
	$(window).resize(function(){
	    window.resizeTo(size[0],size[1]);
	});
</script>

<script type="text/javascript">
	



</script>

<script type="text/javascript">

   (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-59541304-1', 'auto');
  ga('send', 'pageview');
</script>

</body>
</html>
