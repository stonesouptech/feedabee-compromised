<?php 
/* 
	Template Name: Education
*/

get_header();
$urls = [
	'http://goo.gl/2OsPLc',
	'http://goo.gl/hHVdfA',
	'http://goo.gl/1gOhHS',
	'http://goo.gl/XSrsHH',
	'http://goo.gl/UFucmH',
];

$mesages = [
	'Help+feed+a+bee%21+Learn+how+you+can+help+%23FeedABee+at+feedabee.com',
	'Bees+help+feed+us%21+Learn+how+you+can+help+%23FeedABee+at+feedabee.com',
	'In+2015%2C+we+planted+over+65+million+flowers%21+Learn+how+you+can+continue+to+plant+and+%23FeedABee+at+feedabee.com',
	'Let%E2%80%99s+keep+planting+more+flowers+for+bees%21+Learn+more+about+%23FeedABee+at+feedabee.com',
	'Bee+informed+about+pollinators.++Learn+how+you+can+help+%23FeedABee+at+feedabee.com',
];

?>

<div class="ed-content">
	<h1 class="text-center">Education</h1>
	<?php $i=0; ?>
	<?php foreach(get_field('question') as $question): ?>
		<?php $i+=1; ?>
		<div class="ed-section">
			<div class="ed-image col-sm-6">
				<div class="table-cell">
					<img src="<?php echo $question['image']; ?>" dynsrc="<?php echo $question['image']; ?>" alt=""  loop=infinite>
				</div>
			</div>
			<div class="context col-sm-6">
				<div class="title"><h2><?php echo $question['title']; ?></h2></div>
				<div class="answer"><?php echo $question['answer']; ?></div>
				<!-- <div class="social">
					Share the Buzz
					<div class="share">
						<a target ="_blank" href="https://twitter.com/intent/tweet?&text=<?php echo $mesages[$i-1] ?>+🐝&url=<?php echo $urls[$i-1]; ?>" class="share-icons twitter section_twitter" id="twitter_section_<?php echo $i; ?>" title="<?php echo $question['title']; ?>"></a>
						<span class="s-divider"></span>
						<a href="javascript:void(0)" class="share-icons facebook" id="facebook_section_<?php echo $i; ?>"></a>
						<span class="s-divider"></span>
						<a target ="_blank" class="share-icons tumblr section-tumblr" id="tumblr_section_<?php echo $i; ?>" href="http://www.tumblr.com/share/photo?source=http%3A%2F%2Fimg.feedabee.com%2Fcards%2Fsection<?php echo $i; ?>.gif&caption=<?php echo $mesages[$i-1] ?>+🐝&click_thru=http%3A%2F%2Fwww.feedabee.com" title="<?php echo strtoupper(strip_tags($question['title'])); ?>"></a>
					</div>
				</div> -->
			</div>
			<div class="clearfix"></div>
		</div>
	<?php endforeach; ?>
</div>
<div class="clearfix"></div>
<div class="popup freeseeds-popup" style="height: 530px;">
	<div class="popup-header text-right">
		<span class="glyphicon glyphicon-remove close-popup"></span>
	<h3 class="text-center">Get your free packet of seeds to plant!</h3>
	</div>
	<iframe src="http://img.feedabee.com/popup-iframe" style="width:100%;overflow:hidden;height: 440px;border:none"></iframe>
</div>
<div class="popup commit-popup">
	<div class="popup-header text-right"><span class="glyphicon glyphicon-remove close-popup"></span></div>
	<div class="popup-body text-center">
	<img src="<?php echo get_template_directory_uri(); ?>/images/new_pop.jpg" alt="">

		<div class="share">
			<div class="share-text">Share the Buzz</div>
			<hr class="sep">
			<a target="_blank" id="twitter_share_buzz" class="share-icons twitter" href="https://twitter.com/intent/tweet?&amp;text=%20I%20just%20helped%20%23FeedABee%20%26%20you%20can%20too!%20Visit%20www.FeedABee.com%20and%20watch%20the%20video%20to%20learn%20more+🐝&url=http://goo.gl/7mK4Jd" target="_blank"></a>
			<a href="javascript:void(0)" class="share-icons facebook" id="facebook_share_buzz"></a>
			<a target="_blank" id="tumblr_share_buzz" class="share-icons tumblr" href="http://www.tumblr.com/share/photo?source=http%3A%2F%2Fimg.feedabee.com%2Fcards%2Fpopup1.jpg&caption=I+just+helped+%23FeedABee+%26+you+can+too%21+Visit+www.FeedABee.com+and+watch+the+video+to+learn+how+you+can+support+our+pollinators+🐝&click_thru=http%3A%2F%2Fwww.feedabee.com"></a>
		</div>
	</div>
	<div class="popup-footer"></div>
</div>
<div class="popup thankyou-popup">
	<div class="popup-header text-right"><span class="glyphicon glyphicon-remove close-popup"></span></div>
	<div class="popup-body text-center">
		<div class="share">
			<h3 class="text-center">Thank you for <br> helping #FEEDABEE!</h3>
			<p>With your help, we are increasing forage for pollinators. Share the buzz with your friends and lets keep planting!</p>
			<div class="share-text">Share the Buzz</div>
			<hr class="sep">
			<a target="_blank" id="twitter_thank"  href="https://twitter.com/intent/tweet?&text=I%20just%20helped%20%23FeedABee%20%26%20you%20can%20too!%20Visit%20www.FeedABee.com%20and%20watch%20the%20video%20to%20learn%20more+🐝&url=http://goo.gl/K1ooAv" class="share-icons twitter"></a>
			<a href="javascript:void(0)" class="share-icons facebook" id="facebook_thank"></a>
			<a class="share-icons tumblr" id="tumblr_thank" target="_blank" href="http://www.tumblr.com/share/photo?source=http%3A%2F%2Fimg.feedabee.com%2Fcards%2Fpopup2.jpg&caption=I+just+helped+%23FeedABee+%26+you+can+too%21+Visit+www.FeedABee.com+and+watch+the+video+to+learn+how+you+can+support+our+pollinators+🐝&click_thru=http%3A%2F%2Fwww.feedabee.com"></a>
		</div>
	</div>
	<div class="popup-footer"></div>
</div>
<div class="overlay"></div>


<script>

$(document).ready(function(){
	var eventMethod = window.addEventListener ? "addEventListener" : "attachEvent";
	var eventer = window[eventMethod];
	var messageEvent = eventMethod == "attachEvent" ? "onmessage" : "message";

	// Listen to message from child window
	eventer(messageEvent,function(e) {
	    var key = e.message ? "message" : "data";
	    var data = e[key];
	    if(data == 'closePopUp'){
	    	$('.freeseeds-popup .close-popup').click();
	    }
	    //run function//
	},false);
});


if(location.hostname == 'feedabe3.tumblr.com'){
  window.fbAsyncInit = function() {
     FB.init({
	      appId      : '1148056401919842',
	      xfbml      : true,
	      version    : 'v2.5'
	    });
    
  };
}else{
	window.fbAsyncInit = function() {
	   FB.init({
	      appId      : '1079085332137642',
	      xfbml      : true,
	      version    : 'v2.5'
	    });
	};
}

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));
</script>
<script type="text/javascript">
  $(document).ready(function(){
    $('#facebook_section_1').click(function(e){
      e.preventDefault();
      ga('send', {
		  	hitType: 'event',
		    eventCategory: 'Shares',
		    eventAction: 'shareFacebook',
		    eventLabel: 'Facebook: HELP FEED A BEE...',
		  });
      FB.ui(
        {
          method: 'feed',
          name: 'HELP FEED A BEE...',
          link: '<?php echo get_permalink(45)?>',
          picture: '<?php echo get_template_directory_uri(); ?>/images/section-1.gif',
          caption: 'FeedABee',
          description: 'Help feed a bee! Learn how you can help #FeedABee at feedabee.com 🐝',
          message: ''
        }
      );
    });

    $('#facebook_section_2').click(function(e){
      e.preventDefault();
      ga('send', {
		  	hitType: 'event',
		    eventCategory: 'Shares',
		    eventAction: 'shareFacebook',
		    eventLabel: 'Facebook: ...BECAUSE BEES HELP FEED US',
		  });
      FB.ui(
        {
          method: 'feed',
          name: '...BECAUSE BEES HELP FEED US',
          link: '<?php echo get_permalink(45)?>',
          picture: '<?php echo get_template_directory_uri(); ?>/images/section-2.gif',
          caption: 'FeedABee',
          description: 'Bees help feed us! Learn how you can help #FeedABee at feedabee.com 🐝',
          message: ''
        }
      );
    });

    $('#facebook_section_3').click(function(e){
      e.preventDefault();
      ga('send', {
		  	hitType: 'event',
		    eventCategory: 'Shares',
		    eventAction: 'shareFacebook',
		    eventLabel: 'Facebook: IN 2015, WE PLANTED OVER 65 MILLION FLOWERS!',
		  });
      FB.ui(
        {
          method: 'feed',
          name: 'IN 2015, WE PLANTED OVER 65 MILLION FLOWERS!',
          link: '<?php echo get_permalink(45)?>',
          picture: '<?php echo get_template_directory_uri(); ?>/images/section-3.gif',
          caption: 'FeedABee',
          description: 'In 2015, we planted over 65 million flowers! Learn how you can continue to plant and #FeedABee at feedabee.com 🐝',
          message: ''
        }
      );
    });

    $('#facebook_section_4').click(function(e){
      e.preventDefault();
      ga('send', {
		  	hitType: 'event',
		    eventCategory: 'Shares',
		    eventAction: 'shareFacebook',
		    eventLabel: 'Facebook: LET’S KEEP PLANTING MORE!',
		  });
      FB.ui(
        {
          method: 'feed',
          name: 'LET’S KEEP PLANTING MORE!',
          link: '<?php echo get_permalink(45)?>',
          picture: '<?php echo get_template_directory_uri(); ?>/images/section-4.gif',
          caption: 'FeedABee',
          description: 'Let’s keep planting more flowers for bees! Learn more about #FeedABee at feedabee.com 🐝',
          message: ''
        }
      );
    });

    $('#facebook_section_5').click(function(e){
      e.preventDefault();
      ga('send', {
		  	hitType: 'event',
		    eventCategory: 'Shares',
		    eventAction: 'shareFacebook',
		    eventLabel: 'Facebook: BEE INFORMED ABOUT POLLINATORS',
		  });
      FB.ui(
        {
          method: 'feed',
          name: 'BEE INFORMED ABOUT POLLINATORS',
          link: '<?php echo get_permalink(45)?>',
          picture: '<?php echo get_template_directory_uri(); ?>/images/section-5.gif',
          caption: 'FeedABee',
          description: 'Bee informed about pollinators.Learn how you can help #FeedABee at feedabee.com 🐝',
          message: ''
        }
      );
    });

     $('.section_twitter').click(function(e){
     	var href = $(this).attr('href');
     	var title = $(this).attr('title');

     	ga('send', {
		  	hitType: 'event',
		    eventCategory: 'Shares',
		    eventAction: 'shareTwitter',
		    eventLabel: 'Twitter:'+title,
		  });
     	window.open(href);
     });

    $('.section-tumblr').click(function(e){
     	var href = $(this).attr('href');
     	var title = $(this).attr('title');

     	ga('send', {
		  	hitType: 'event',
		    eventCategory: 'Shares',
		    eventAction: 'shareTumblr',
		    eventLabel: 'Tumblr:'+title,
		  });
     	window.open(href);
     });

    $('#twitter_share_buzz').click(function(e){
     	var href = $(this).attr('href');
     	ga('send', {
		  	hitType: 'event',
		    eventCategory: 'Shares',
		    eventAction: 'shareTwitter',
		    eventLabel: 'share List Popup Twitter Education',
		  });
     	window.open(href);
    });

    $('#tumblr_share_buzz').click(function(e){
     	var href = $(this).attr('href');
     	ga('send', {
		  	hitType: 'event',
		    eventCategory: 'Shares',
		    eventAction: 'shareTumblr',
		    eventLabel: 'share List Popup Tumblr Education',
		  });
     	window.open(href);
    });

     $('#facebook_share_buzz').click(function(e){
      e.preventDefault();
      ga('send', {
	  	hitType: 'event',
	    eventCategory: 'Shares',
	    eventAction: 'shareFacebook',
	    eventLabel: 'share List Popup Facebook Education',
	  });
      FB.ui(
        {
          method: 'feed',
          name: 'HELP INCREASE THE NUMBER OF BEES BY PLANTING ONE OF THESE POLINATOR FRIENDLY FLOWERS',
          link: 'http://feedabee.com',
          picture: '<?php echo get_template_directory_uri(); ?>/images/FAB-list-of-seeds-social-share-from-pop-up.jpg',
          caption: 'FeedABee',
          description: 'I just helped #FeedABee & you can too! Visit www.FeedABee.com and watch the video to learn how you can support our pollinators 🐝',
          message: ''
        }
      );
    });


    $('#facebook_thank').click(function(e){
      e.preventDefault();
      ga('send', {
	  	hitType: 'event',
	    eventCategory: 'Shares',
	    eventAction: 'shareFacebook',
	    eventLabel: 'share Thanks Popup Facebook Home',
	  });
      FB.ui(
        {
          method: 'feed',
          name: 'THANK YOU FOR HELPING #FEEDABEE!',
          link: 'http://feedabee.com',
          picture: '<?php echo get_template_directory_uri(); ?>/images/FAB-thank-you-social-share-from-pop-up.jpg',
          caption: 'FeedABee',
          description: 'I just helped #FeedABee & you can too! Visit www.FeedABee.com and watch the video to learn how you can support our pollinators 🐝',
          message: ''
        }
      );
    });

     $('#twitter_thank').click(function(e){
     	var href = $(this).attr('href');
     	ga('send', {
		  	hitType: 'event',
		    eventCategory: 'Shares',
		    eventAction: 'shareTwitter',
		    eventLabel: 'share Thanks Popup Twitter Home',
		  });
     	window.open(href);
     });

     $('#tumblr_thank').click(function(e){
     	var href = $(this).attr('href');
     	ga('send', {
		  	hitType: 'event',
		    eventCategory: 'Shares',
		    eventAction: 'shareTumblr',
		    eventLabel: 'share Thanks Popup Tumblr Home',
		  });
     	window.open(href);
    });

  });
</script>
<script>
	imageCenter();

	$(window).on('resize', function(){
		imageCenter();
	});

	function imageCenter() {
		if($(window).width() > 768){
			$('.ed-section').each(function(index, value){
				$(this).children('.ed-image').height($(this).height());
			});
		}
	}
	$('a[href="#order"]').on('click', function(e){
		e.preventDefault();
		$('.overlay').fadeIn(300);
		$('.popup.freeseeds-popup').fadeIn(300);
	});
	$('a[href="#plant-list"]').on('click', function(e){
		e.preventDefault();
		
		$('.overlay').fadeIn(300);
		$('.popup.commit-popup').fadeIn(300);
	});

	$('.close-popup, .overlay').on('click', function(){
		if(!$('.overlay').hasClass('thankyou')){
			$('.popup').fadeOut(300);
			$('.thankyou-popup').fadeIn(300);
			$('.overlay').addClass('thankyou');
		} else {
			$('.overlay').fadeOut(300);
			$('.popup').fadeOut(300);
			$('.overlay').removeClass('thankyou');
		}
	});

	if($(window).width() > 768){
			$('.to-center').height($('.to-center-after').height());
		}
</script>
<?php get_footer(); ?>