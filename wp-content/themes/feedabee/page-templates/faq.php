<?php 

/* 
	Template Name: FAQ

*/


get_header();


$i = 0;
foreach ( get_field('question') as $faq ) :; 
	if($i%2 == 0){ ?>
		
		<div class="row-question">
			<div class="image-faq text-left">
				<?php if ( $faq['image'] ): ?>
					<img src="<?php echo $faq['image'] ?>" alt="" >
				<?php endif; ?>
			</div>
			<div class="question right <?php if(count(get_field('question')) == $i+1)  echo 'last-q'; else echo ''; ?>">
				<h2><?php echo $faq['title'] ?></h2>
				<?php echo $faq['answer'] ?>
			</div>
			<div class="clearfix"></div>
		</div>
<?php 
	}else{ 
?>
		<div class="row-question">
			<div class="image-faq right text-right">
				<?php if ( $faq['image'] ): ?>
					<img src="<?php echo $faq['image'] ?>" alt="" >
				<?php endif; ?>
			</div>
			<div class="question left <?php if(count(get_field('question')) == $i+1)  echo 'last-q'; else echo ''; ?>">
				<h2><?php echo $faq['title'] ?></h2>
				<?php echo $faq['answer'] ?>
			</div>
			<div class="clearfix"></div>
		</div>
<?php
	}
	$i++;
endforeach; 
?>

<?php get_footer(); ?>