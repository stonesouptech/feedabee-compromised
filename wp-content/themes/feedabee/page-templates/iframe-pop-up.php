<?php 
/* 
	Template Name: Iframe

*/
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Bees and pollinators need to eat! With the help of our partners and people like you, we're going to plant millions of seeds and create forage for pollinators across the country! It's easy to help plant seeds to feed a bee. ">
	<meta name="keywords" content="grow,plant,free,seeds,garden,bee health,bees,pollinator,flower,wildflower">	
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<?php wp_head(); ?>
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/jquery.mCustomScrollbar.min.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/main.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/main2.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/responsive.css">
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,800,700' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700' rel='stylesheet' type='text/css'>
	<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.ico" type="image/x-icon">
	<script src="https://code.jquery.com/jquery-2.2.0.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/main.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.js" ></script>
</head>
<style type="text/css">
	body{
		background: #1accca;
		overflow: hidden;
	}
	.popup-body {
	    background-color: #1accca;
	    font-family: 'PT Sans Caption', sans-serif;
	    padding: 20px;
	    color: #fff;
	}
	#wpadminbar{
		display:none;
	}

	input[name="state"] {
	    width: 25%;
	    display: inline-block;
	}

	input[name="city"] {
	    width: calc(75% - 15px);
	    margin-right: 10px;
	    display: inline-block;
	        float: left;
	}

	input[name="zip"] {
	    width: calc(66% - 15px);
	    display: inline-block;
	    margin-right: 10px;
	        float: left;
	}

	input[type="submit"] {
	    width: 34%;
	    background: none;
	    display: inline-block;
	    padding: 0;
	    height: 35px;
	    line-height: 36px;
	    text-align: center;
	    float: left;
    	font-family: 'CooperHewitt';
		letter-spacing: 3px;
    	font-size: 16px;
    	text-transform: uppercase;
    	margin-top: 15px;
    	border: 1px solid #fff;
    	-webkit-transition: .3s all ease-out;
	    -moz-transition: .3s all ease-out;
	    -ms-transition: .3s all ease-out;
	    -o-transition: .3s all ease-out;
	    transition: .3s all ease-out;

	}

	input[type="submit"]:hover{
		color: #1accca;
		text-decoration: none;
    	background-color: #fff;
	}

	.updates p {
	    font-weight: normal;
	    display: inline;
	}

	.updates input[type="checkbox"] {
	    display: none;
	}

	.updates {
	    font-size: 12px;
	}

	html{
		margin-top:0px !important;
	}

	div.wpcf7-response-output{
		margin-top:0px;
	}

	table, td, tr, th, tbody{
		display: block;	
		width:auto;
	}

	input{
		text-align:center;
	}

	th{
		display:none;
	}
	button, input, select, textarea {
	    max-width: 100%;
	}

	.glyphicon.glyphicon-remove:after{
		display:none;
	}

	.glyphicon {
    position: relative;
    top: 1px;
    display: inline-block;
    font-family: 'Glyphicons Halflings' !important;
    font-style: normal !important;
    font-weight: 400 !important;
    line-height: 1;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;
}

.pdb-thanks{
	margin-top:20px;
}


</style>
<body <?php body_class(); ?>>
	<div class="popup-body text-center">
		Get your seed packet to plant and support pollinator health in your own neighborhood. Don’t forget to check back in for planting tips. Please share photos of progress of your seeds using #FeedABee.
	 	<?php echo do_shortcode('[pdb_signup]');?>
	 	<div class="updates"><input type="checkbox" name="address" id="address" checked=""><label for="address" id="updates_pop_1">
		<div class="checkbox"><span class="glyphicon glyphicon-remove"></span></div>
		<p> I would like to receive bee updates from Bayer</p></label></div>
	</div>
<script>
	$(document).ready(function(){

		$('input[name=updates_bayer]').val(1);
		$('.pdb-signup .checkbox').prop('checked', true);

		$('#updates_pop_1').click(function(){
			if( $(this).find('#address').is(':checked') ){
				$('input[name=updates_bayer]').val(1);
				$('.pdb-signup .checkbox').prop('checked', true);
			}else{
				$('input[name=updates_bayer]').val(0);
				$('.pdb-signup .checkbox').prop('checked', false);
			}
		});

		if($('.pdb-thanks').size() != 0){
			$('.updates').hide();
		}

		$('input[name=name_user]').attr('placeholder', 'NAME');
		$('input[name=email]').attr('placeholder', 'EMAIL');
		$('input[name=street_address]').attr('placeholder', 'STREET ADDRESS');
		$('input[name=city]').attr('placeholder', 'CITY');
		$('input[name=state]').attr('placeholder', 'STATE');
		$('input[name=zip]').attr('placeholder', 'ZIP');

		$('input[type="submit"]').click(function(){

			parent.postMessage("closePopUp","*");
		});
	});
</script>
<?php wp_footer(); ?>

</body>
</html>
