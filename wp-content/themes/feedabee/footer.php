<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

if (strstr($_SERVER['REQUEST_URI'],'/education/') !== false) { ?>
<br>
			<div class="footer-share">
				<div class="footer-top-el">
					<h3>#FEEDABEE</h3>
					<img class="bee-footer-icon resp-bee-icon" src="<?php echo get_template_directory_uri(); ?>/images/bw-bee.png" alt="bee">
				</div>
				<ul>
					<li><img class="bee-footer-icon" src="<?php echo get_template_directory_uri(); ?>/images/bw-bee.png" alt="bee"></li>
					<li><a class="twitter-footer-education" target="_blank" href="https://twitter.com/intent/tweet?&text=Learn%20more%20about%20how%20%23FeedABee%20%F0%9F%90%9D%20is%20supporting%20pollinators!%20&url=http://feedabee.stonesoupdev.com/education"><img class="hvr-grow" src="<?php echo get_template_directory_uri(); ?>/images/twitter-black.png" alt="twitter"></a></li>
					<li><a href="javascript:void(0)" id="share-facebook-education"><img class="hvr-grow" src="<?php echo get_template_directory_uri(); ?>/images/facebook-black.png" alt="facebook"></a></li>
					<li><a class="tumblr-footer-education" target="_blank" href="http://www.tumblr.com/share/photo?source=http%3A%2F%2Ffeedabee.stonesoupdev.com%2Fwp-content%2Fthemes%2Ffeedabee%2Fimages%2FFacebook-Share-Card-4-EDUCATION-PAGE-240x240.jpg&caption=Learn%20how%20%23FeedABee%20%F0%9F%90%9D%20is%20educating%20on%20the%20importance%20of%20supporting%20bees%20and%20other%20pollinators.%20Join%20us%20in%20learning%20more%20about%20how%20we%20can%20support%20our%20pollinators.&click_thru=http%3A%2F%2Fwww.feedabee.com%2Feducation"><img class="hvr-grow" src="<?php echo get_template_directory_uri(); ?>/images/tumblr-black.png" alt="tumblr"></a></li>
				</ul>
			</div>
			<?php } else if (strstr($_SERVER['REQUEST_URI'],'/impact/') !== false) { ?>
<br>
			<div class="footer-share">
				<div class="footer-top-el">
					<h3>#FEEDABEE</h3>
					<img class="bee-footer-icon resp-bee-icon" src="<?php echo get_template_directory_uri(); ?>/images/bw-bee.png" alt="bee">
				</div>
				<ul>
					<li><img class="bee-footer-icon" src="<?php echo get_template_directory_uri(); ?>/images/bw-bee.png" alt="bee"></li>
					<li><a class="twitter-footer-impact" target="_blank" href="https://twitter.com/intent/tweet?&text=Take%20a%20look%20at%20how%20%23FeedABee%20%F0%9F%90%9D%20is%20planting%20forage%20in%20all%2050%20states!%20&url=http://feedabee.stonesoupdev.com/impact"><img class="hvr-grow" src="<?php echo get_template_directory_uri(); ?>/images/twitter-black.png" alt="twitter"></a></li>
					<li><a href="javascript:void(0)" id="share-facebook-impact"><img class="hvr-grow" src="<?php echo get_template_directory_uri(); ?>/images/facebook-black.png" alt="facebook"></a></li>
					<li><a class="tumblr-footer-impact" target="_blank" href="http://www.tumblr.com/share/photo?source=http%3A%2F%2Ffeedabee.stonesoupdev.com%2Fwp-content%2Fthemes%2Ffeedabee%2Fimages%2FFacebook-Share-Card-5-IMPACT-PAGE-240x240.jpg&caption=%23FeedABee%20is%20dedicated%20to%20planting%20forage%20in%20all%2050%20states%20by%20the%20end%20of%202018.%20Check%20out%20how%20many%20states%20have%20been%20impacted%20so%20far.&click_thru=http%3A%2F%2Fwww.feedabee.com%2Fimpact"><img class="hvr-grow" src="<?php echo get_template_directory_uri(); ?>/images/tumblr-black.png" alt="tumblr"></a></li>
				</ul>
			</div>
			<?php } ?>


		</div> 

<?php if (strstr($_SERVER['REQUEST_URI'],'/education/') !== false) { ?>

<div class="pre-footer">
	<div class="col-sm-3 to-center" style="height: 48px;">
		<div class="table-cell">
			<img src="<?php echo get_template_directory_uri(); ?>/images/fab-bayer.png" alt="">
		</div>
	</div>
	<div class="col-sm-9 to-center-after">
		The Feed a Bee campaign is supported by the Bayer Bee Health program.  For more information on bee health, please visit <a href="http://www.beehealth.bayer.us" style="font-size: 14px; font-weight: bold" class="green-link" target="_blank">beehealth.bayer.us</a>.
	</div>
	<div class="clearfix"></div>
</div>

<?php } ?>

			<div class="footer text-center">
				Bayer Crop Science LP, 2 T.W. Alexander Drive, Research Triangle Park, N.C. 27709. Always read and follow label instructions. Bayer and the Bayer Cross are registered trademarks of Bayer. For product information, call toll-free 1-866-99-BAYER (1-866-992-2937) or visit our website at <a style="color:#a9b008" href="http://www.cropscience.bayer.us" target="_blank">www.cropscience.bayer.us</a>
			</div><br><!-- /div.content -->
	</div> <!-- /div.container -->
</div> <!-- /div#page -->


<div class="popup" id="popup-thanks-generic">
  <a href="javascript:void(0)" class="close_top"></a>
  <a href="javascript:void(0)" class="close_bottom"></a>
</div>

<?php wp_footer(); ?>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-59541304-1', 'auto');
  ga('send', 'pageview');

</script>

<script type="text/javascript">


  	 $('.twitter-footer-education').click(function(e){
  	  	 show_popup($('#popup-thanks-generic'));
	       ga('send', {
			  hitType: 'event',
			  eventCategory: 'Shares',
			  eventAction: 'shareTwitter',
			  eventLabel: 'share Footer Twitter Education'
			});
  	 });

  	 $('.tumblr-footer-education').click(function(e){
  	  	 show_popup($('#popup-thanks-generic'));
	       ga('send', {
			  hitType: 'event',
			  eventCategory: 'Shares',
			  eventAction: 'shareTumblr',
			  eventLabel: 'share Footer Tumblr Education'
			});
  	 });

  	 $('.twitter-footer-impact').click(function(e){
  	  	 show_popup($('#popup-thanks-generic'));
	       ga('send', {
			  hitType: 'event',
			  eventCategory: 'Shares',
			  eventAction: 'shareTwitter',
			  eventLabel: 'share Footer Twitter Impact'
			});
  	 });

  	 $('.tumblr-footer-impact').click(function(e){
  	  	 show_popup($('#popup-thanks-generic'));
	       ga('send', {
			  hitType: 'event',
			  eventCategory: 'Shares',
			  eventAction: 'shareTumblr',
			  eventLabel: 'share Footer Tumblr Impact'
			});
  	 });

	if(location.hostname == 'feedabee.com'){
  window.fbAsyncInit = function() {
     FB.init({
	      appId      : '1148056401919842',
	      xfbml      : true,
	      version    : 'v2.5'
	    });

  };
}else{
	window.fbAsyncInit = function() {
	   FB.init({
	      appId      : '769761559841387',
	      xfbml      : true,
	      version    : 'v2.5'
	    });
	};
}

  (function(d, s, id){
     var js, fjs = d.getElementsByTagName(s)[0];
     if (d.getElementById(id)) {return;}
     js = d.createElement(s); js.id = id;
     js.src = "//connect.facebook.net/en_US/sdk.js";
     fjs.parentNode.insertBefore(js, fjs);
   }(document, 'script', 'facebook-jssdk'));

	function show_popup(popup) {
		if (popup) {
			popup.fadeIn();
		}
	}

	function hide_popup(popup) {
		if (popup) {
			popup.fadeOut();
		}
	}

	$(document).on("click",".popup",function() {
		hide_popup($(this));
	});

     $('#share-facebook-education').click(function(e){
      e.preventDefault();
      FB.ui(
        {
          method: 'feed',
          name: 'FeedABee',
          link: 'http://feedabee.com',
          picture: '<?php echo get_template_directory_uri(); ?>/images/Facebook-Share-Card-4-EDUCATION-PAGE-240x240.jpg',
          caption: 'FeedABee.com',
          description: 'Learn how #FeedABee is educating on the importance of supporting bees and other pollinators. Join us in learning more about how we can support our pollinators.',
          message: '#FeedABee 🐝'
        }
      );
      show_popup($('#popup-thanks-generic'));

      ga('send', {
		  hitType: 'event',
		  eventCategory: 'Shares',
		  eventAction: 'shareFacebook',
		  eventLabel: 'share Footer Facebook Education'
		});
    });

     $('#share-facebook-impact').click(function(e){
      e.preventDefault();
      FB.ui(
        {
          method: 'feed',
          name: 'FeedABee & all 50 State',
          link: 'http://feedabee.com',
          picture: '<?php echo get_template_directory_uri(); ?>/images/Facebook-Share-Card-5-IMPACT-PAGE-240x240.jpg',
          caption: 'FeedABee.com',
          description: '#FeedABee is dedicated to planting forage in all 50 states by the end of 2018. Check out how many states have been impacted so far.',
          message: '#FeedABee 🐝'
        }
      );
      show_popup($('#popup-thanks-generic'));

      ga('send', {
		  hitType: 'event',
		  eventCategory: 'Shares',
		  eventAction: 'shareFacebook',
		  eventLabel: 'share Footer Facebook Impact'
		});
    });

</script>

</body>
</html>
