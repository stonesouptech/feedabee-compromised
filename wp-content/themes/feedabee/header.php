<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Bees and pollinators need to eat! With the help of our partners and people like you, we're going to plant millions of seeds and create forage for pollinators across the country! It's easy to help plant seeds to feed a bee. ">
	<meta name="keywords" content="grow,plant,free,seeds,garden,bee health,bees,pollinator,flower,wildflower">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php endif; ?>
	<?php wp_head(); ?>
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/bootstrap.min.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/jquery.mCustomScrollbar.min.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/main.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/main2.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/responsive.css">
	<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,800,700' rel='stylesheet' type='text/css'>
	<link href='https://fonts.googleapis.com/css?family=PT+Sans:400,700' rel='stylesheet' type='text/css'>
	<link rel="shortcut icon" href="<?php echo get_template_directory_uri(); ?>/images/favicon.ico" type="image/x-icon">
	<script src="https://code.jquery.com/jquery-2.2.0.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/main.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/bootstrap.js" ></script>

	<?php 
if (strstr($_SERVER['REQUEST_URI'],'/education/') !== false) { ?>


	<meta property="og:title" content="FeedABee" />
	<meta property="og:site_name" content="FeedABee"/>
	<meta property="og:image" content="<?php echo get_template_directory_uri(); ?>/images/Twitter-Share-Card-4-EDUCATION-PAGE-560x300.jpg" />
	<meta property="og:description" content="Learn how #FeedABee 🐝 is educating on the importance of supporting bees and other pollinators. Join us in learning more about how we can support our pollinators." />
	<meta property="og:url" content="http://feedabee.com/" />

	<meta name="twitter:card" content="summary_large_image" />
	<meta name="twitter:site" content="@FeedABee" />
	<meta name="twitter:title" content="FeedABee" />
	<meta name="twitter:description" content="Learn how #FeedABee 🐝 is educating on the importance of supporting bees and other pollinators. Join us in learning more about how we can support our pollinators." />
	<meta name="twitter:text:description" content="Learn how #FeedABee 🐝 is educating on the importance of supporting bees and other pollinators. Join us in learning more about how we can support our pollinators." />
	<meta name="twitter:image:src" content="<?php echo get_template_directory_uri(); ?>/images/Twitter-Share-Card-4-IMPACT-PAGE-560x300.jpg" />
	<meta name="twitter:url" content="http://feedabee.com/" />
<?php } else if (strstr($_SERVER['REQUEST_URI'],'/impact/') !== false) { ?>


	<meta property="og:title" content="FeedABee & all 50 States" />
	<meta property="og:site_name" content="FeedABee"/>
	<meta property="og:image" content="<?php echo get_template_directory_uri(); ?>/images/Twitter-Share-Card-5-IMPACT-PAGE-560x300.jpg" />
	<meta property="og:description" content="#FeedABee is dedicated to planting forage in all 50 states by the end of 2018. Check out how many states have been impacted so far. " />
	<meta property="og:url" content="http://feedabee.com/" />

	<meta name="twitter:card" content="summary_large_image" />
	<meta name="twitter:site" content="@FeedABee" />
	<meta name="twitter:title" content="FeedABee & all 50 States" />
	<meta name="twitter:description" content="#FeedABee is dedicated to planting forage in all 50 states by the end of 2018. Check out how many states have been impacted so far. " />
	<meta name="twitter:text:description" content="#FeedABee is dedicated to planting forage in all 50 states by the end of 2018. Check out how many states have been impacted so far. " />
	<meta name="twitter:image:src" content="<?php echo get_template_directory_uri(); ?>/images/Twitter-Share-Card-5-IMPACT-PAGE-560x300.jpg" />
	<meta name="twitter:url" content="http://feedabee.com/" />
<?php } ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<div class="header-top">
		<div class="container">
			<div class="row">
				<div class="logo col-xs-3 col-sm-3">
					<a href="http://feedabee.com"><img src="<?php echo get_template_directory_uri(); ?>/images/logo.png" alt=""></a>
				</div>
				<div class="col-xs-9 col-sm-9 text-right left-content-header">
					<nav class="navbar navbar-default">
						<div class="navbar-header">
					      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#menu-main-menu-container" aria-expanded="false">
					        <span class="sr-only">Toggle navigation</span>
					        <span class="icon-bar"></span>
					        <span class="icon-bar"></span>
					        <span class="icon-bar"></span>
					      </button>
					    </div>
					</nav>
						<?php wp_nav_menu(['link_after' => '<li class="sep"> | </li>', 'container_class'=>'collapse navbar-collapse menu-container no-transition', 'menu_class'=>'menu', 'container_id'=>'menu-main-menu-container']); ?>
					<div class="bayer-logo">
						<img src="<?php echo get_template_directory_uri(); ?>/images/bayer-logo.png" alt="">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="header" style="background-image:url(<?php the_field('page_header_image'); ?>);">
		<div class="container">
			<div class="heading text-center">
<!--				<h1>--><?php //the_title(); ?><!--</h1>-->
			</div>
		</div>
		<div class="bottom-pattern">
			<img src="<?php echo get_template_directory_uri(); ?>/images/bottom-pattern.png" alt="">
		</div>
	</div>
	<div class="container sm-container">
		<div class="content">
