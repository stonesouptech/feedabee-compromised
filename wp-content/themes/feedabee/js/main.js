
//add remove sticky class
function sticky_header(){
	var scrollTop = $(window).scrollTop();
	if( scrollTop > 0 ){
		if(!$('.header-top').hasClass('sticky')){
			$('.header-top').addClass('sticky');
		}
	}else{
		if($('.header-top').hasClass('sticky')){
			$('.header-top').removeClass('sticky');
		}
	}
}

$(document).ready(function(){
	sticky_header();
	var lastScrollTop = 0;
	$(window).scroll(function(event){
	   var st = $(this).scrollTop();
	   if (st > lastScrollTop){
	       $('.header-top').removeClass('sticky');
	   } else {
	     sticky_header();
	   }
	   lastScrollTop = st;
	});
});

