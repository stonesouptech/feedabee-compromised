<?php
$server = isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '';

$image = "http://feedabee.stonesoupdev.com/cards/billion.jpg";
$url  = "http://feedabee.com/";
$title = "FeedABee";
$description = 'Every time #FeedABee 🐝 is shared, we’ll plant a wildflower. With your help, we want to plant more flowers to create forage for pollinators around the U.S. ';
if (strpos($server, "Twitterbot") === false) {
	header('Location: '.$url);
	die();
}
?><html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0">
<meta http-equiv="Access-Control-Allow-Origin" content="http://whitesunrisetesting.com">
<title>Feed A Bee</title>

<link rel="shortcut icon" href="<?=$image?>">
<link rel="apple-touch-icon" href="<?=$image?>">
<link rel="alternate" type="application/rss+xml" href="http://feedabee.tumblr.com/rss">

<meta property="og:title" content="<?php echo $title; ?>" />
<meta property="og:site_name" content="<?php echo $title; ?>"/>
<meta property="og:url" content="<?php echo $url?>" />
<meta property="og:image" content="<?php echo $image?>" />
<meta property="og:description" content="<?php echo $description?>" />

<meta name="twitter:card" content="summary_large_image" />
<meta name="twitter:site" content="@FeedABee" />
<meta name="twitter:title" content="<?=$title?>" />
<meta name="twitter:description" content="<?php echo $description?>" />
<meta name="twitter:text:description" content="<?php echo $description?>" />
<meta name="twitter:image:src" content="<?php echo $image?>" />
<meta name="twitter:url" content="http://feedabee.com/" />
<meta property="og:url" content="http://feedabee.com/" />
<style>
body {
	margin:0px;
	padding:0px;
}
#header {
	position: relative;
	width: 100%;
	min-height: 114px;
	z-index: 10;
	background-image: url("http://img.favpetpic.com/img/header-rip.png");
	background-position: top center;
	background-repeat: repeat-x;
	text-align: center;
	padding: 0;
}
</style>
</head>
<body>

</body>
</html>
